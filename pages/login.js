import { useState } from "react";
import Layout from "../components/layout";
import Seo from "../components/seo";

import { fetchAPI } from "../lib/api";


const Login = ({ categories }) => {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")


  const seo = {
    // metaTitle: category.attributes.name,
    // metaDescription: `All ${category.attributes.name} articles`,
  };

  return (
    <Layout categories={categories.data}>
      <Seo seo={seo} />
      <div className="uk-section">
        <div className="uk-container uk-container-large">
          {/* <h1>{category.attributes.name}</h1>
          <Articles articles={category.attributes.articles.data} /> */}
            <div>
                <label>Email</label>
                <input />
            </div>
            <div>
                <label>Password</label>
                <input />
            </div>
        </div>
      </div>
    </Layout>
  );
};

export async function getStaticProps({ params }) {
  const allCategories = await fetchAPI("/categories");

  return {
    props: {
      categories: allCategories,
    },
    revalidate: 1,
  };
}

export default Login