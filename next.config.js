/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    loader: "default",
    domains: ["localhost", "127.0.0.1", "content.fp.tartu.tech"],
  },
}

module.exports = nextConfig
